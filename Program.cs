﻿namespace TPCercle;

class Program
{
    static void Main(string[] args)
    {

        Console.WriteLine("Donner l'abscisse du centre:");
        int x= Convert.ToInt16(Console.ReadLine());
        Console.WriteLine("Donner l'ordonnée du centre:");
        int y = Convert.ToInt16(Console.ReadLine());
        Console.WriteLine("Donner le rayon:");
        int r = Convert.ToInt16(Console.ReadLine());

        //creation d'instance Point
        Point pt1= new Point(x,y);
     
        //meme parametre que son constructeur
        Cercle cercle= new Cercle(pt1,r);
        cercle.Display();
        cercle.getPerimeter();
        cercle.getSurface();

        Console.WriteLine("Entrez les coordonnées d'un point (x y) pour vérifier s'il appartient au cercle :");
        
        int pointX = Convert.ToInt16(Console.ReadLine());
        int pointY = Convert.ToInt16(Console.ReadLine());

        Point point = new Point(pointX, pointY);

        if (cercle.IsInclude(point))
            Console.WriteLine("Le point appartient au cercle.");
        else
            Console.WriteLine("Le point n'appartient pas au cercle.");
    }

}
   
    

class Point
{
    //attribut x et y
    public int X { get; set; }
    public int Y { get; set; }

    //Constructeur 
    public Point(int x, int y){
        X = x;
        Y = y;
    }

    //Methode Display
    public void Display(){
        Console.WriteLine($"POINT({X},{Y})");
    } 
}
 
class Cercle{
    //attribut de type classe 
    public Point  Monpoint {get; set;} 
    public int Rayon {get; set;}
   

    //constructeur 
    public Cercle(Point monpoint, int rayon){
        Monpoint = monpoint;
        Rayon = rayon;
    }

    //Methode getPerimeter()
    public void getPerimeter(){

        double perimeter= 2 * Math.PI * Rayon;
        Console.WriteLine($"Le périmètre est:{Math.Round(perimeter, 2)}" );  

    }

    //Methode getSurface()
    public void getSurface()
    {

        double surface = Math.PI * Rayon * Rayon;
        Console.WriteLine($"La surface est:{Math.Round(surface, 2)}");

    }

    //Methode Display()
    public void Display(){
        Console.WriteLine($"CERCLE({Monpoint.X},{Monpoint.Y},{Rayon})");
    }

    public bool IsInclude(Point p)
    {
        double distanceSquared = Math.Pow(p.X - Monpoint.X, 2) + Math.Pow(p.Y - Monpoint.Y, 2);
        double rayonSquared = Math.Pow(Rayon, 2);

        return distanceSquared <= rayonSquared;
    }
}